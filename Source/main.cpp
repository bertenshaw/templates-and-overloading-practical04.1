//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"

template <typename Type>
Type max (Type a, Type b)
{
    if (a < b)
        return b;
    else
        return a;
    
}

template <typename Type>
Type min (Type a, Type b)
{
    if (a > b)
        return b;
    else
        return a;
}

template <typename Type>
Type add (Type a, Type b)
{
    return a + b;
}

template <typename Type>
void   print (Type array[], int size)
{
    for (int count = 0; count < size; count ++)
    {
        std::cout << array[count] << "\n";
    }
}

template <typename Type>
void   getAverage (Type array[], int size)
{
    double average = 0.0; // output initialization
    for (int count = 0; count < size; count ++)
    {
        average = average + array[count];
    }
    average = average / size;
    std::cout << "\nAverage: " << average << "\n";
}

int main ()
{
    double a, b;
    int averageArraySize = 1;
    std::cout << "Enter two values to discover which is the largest\n";

    std::cin >> a;
    std::cin >> b;
    
    std::cout << "\nMaximum:" << max(a, b) <<"\nMinimum:" << min(a, b) << "\nSum:" << add(a, b) << "\nPrint\n";

    double nums[2] = {a, b}; //double array data type used here for compatibility?
    print (nums, 2);
    
    std::cout << "\nNow enter an array size: "; //This is to enter data to 'getAverage'
    std::cin >> averageArraySize;
    std::cout << "\nNow enter each of the values (of which there are " << averageArraySize << "):"; // nice user friendly console info
    
    double averageArray[averageArraySize]; // Array declaration
   
    for (int count = 0; count < averageArraySize; count ++) // populating the array from cin
    {
        std::cout << "Array element: " << count << " Input:";
        std::cin >> averageArray[count];
    }

    getAverage(averageArray, averageArraySize); //prints the average to the cout
    
    
    return 0;
}